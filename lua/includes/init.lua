if SERVER then
    AddCSLuaFile()
end

local function default_lua()
    if SERVER then AddCSLuaFile("sourcedev_default_init.lua") end
    include("sourcedev_default_init.lua")

    if SERVER then AddCSLuaFile("sourcedev_auto_meta.lua") end
    include("sourcedev_auto_meta.lua")
end



if file.Exists("dash/init.lua", "LUA") then
    if (SERVER) then
        AddCSLuaFile 'dash/init.lua'
    end
    include 'dash/init.lua'

    default_lua()

    -- Extensions
    for k, v in pairs(dash.LoadDir('extensions')) do
        dash.IncludeSH(v)
    end
    for k, v in pairs(dash.LoadDir('extensions/server')) do
        dash.IncludeSV(v)
    end
    for k, v in pairs(dash.LoadDir('extensions/client')) do
        dash.IncludeCL(v)
    end
else
    default_lua()
end