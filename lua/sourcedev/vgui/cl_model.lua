local PANEL = {}

AccessorFunc( PANEL, "bRotating", "Rotating", FORCE_BOOL )
AccessorFunc( PANEL, "iRotatingP", "RotatingP", FORCE_NUMBER )
AccessorFunc( PANEL, "iRotatingY", "RotatingY", FORCE_NUMBER )
AccessorFunc( PANEL, "iRotatingR", "RotatingR", FORCE_NUMBER )

function PANEL:Init()
    self:SetRotating(true)
    self:SetRotatingP(0)
    self:SetRotatingY(0)
    self:SetRotatingR(0)
end


function PANEL:LayoutEntity()
    if ( self.bAnimated ) then
        self:RunAnimation()
    end

    
    local min, max = self.Entity:GetModelBounds()
    local center = (min + max)/2

    local minn, maxx = self.Entity:GetModelBoundsFixed()
  
    local s_fov = math.max(maxx.x, maxx.y, maxx.z)
    self:SetLookAt(center)

    local addpos = Vector(0,0,0)
    if self.bRotating then
        local ang = Angle(self.iRotatingP,self.iRotatingY + RealTime() * 50 % 360, self.iRotatingR )
        addpos = ang:Forward()*s_fov*2
    else
        addpos = Angle(self.iRotatingP,self.iRotatingY,self.iRotatingR):Forward()*s_fov*2
    end
    addpos:Add(addpos:GetNormalized()*2)

    self:SetCamPos(center + addpos)
end


vgui.Register("sourcedev.model", PANEL, "DModelPanel")