sourcedev("cache")

MODULE.List = MODULE.List or {}

MODULE.cache_meta = {}
cache_meta.__index = cache_meta
function cache_meta:Clear(funcClear)
    for k, v in pairs(self) do
        if funcClear then funcClear(k, v) end
        self[k] = nil
    end
end

function cache_meta:Insert(...)
    table.insert(self, ...)
end



function MODULE.Get(name)
    if not List[name] then 
        List[name] = {}
        setmetatable(List[name], cache_meta)
    end

    return List[name]
end


MODULE.ListOnceVar = MODULE.ListOnceVar or {}
function MODULE.OnceVar(name, value)
    if ListOnceVar[name] then return ListOnceVar[name] end
    ListOnceVar[name] = value
    return ListOnceVar[name]
end