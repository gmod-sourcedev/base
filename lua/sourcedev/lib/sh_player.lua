sourcedev("player")

MODULE.PlayerData = MODULE.PlayerData or {}
MODULE.ListEntIndex = MODULE.ListEntIndex or {}
MODULE.ListSteamID = MODULE.ListSteamID or {}
MODULE.ListUserID = MODULE.ListUserID or {}
MODULE.ListAddress = MODULE.ListAddress or {}
MODULE.ListName = MODULE.ListName or {}

function getData(userid)
    if not PlayerData[userid] then PlayerData[userid] = {} end
    return PlayerData[userid]
end


function setData(userid, key, value)
    if not PlayerData[userid] then PlayerData[userid] = {} end
    PlayerData[userid][key] = value
end




function onConnect(data)
    local user_data = getData(data.userid)

    local ent_index = data.index + 1
    user_data["index"] = ent_index
    ListEntIndex[ent_index] = data
    user_data["userid"] = data.userid
    ListUserID[data.userid] = user_data

    user_data["name"] = data.name
    MODULE.ListName[data.name] = user_data
    if data.bot == 0 then
        local steamid64 = util.SteamIDTo64(data.networkid)
        user_data["steamid"] = data.networkid
        user_data["steamid64"] = steamid64

        if data.friendsid then
            user_data["steamid3"] = data.friendsid
            ListSteamID[data.friendsid] = user_data
        end

        ListSteamID[steamid64] = user_data
        ListSteamID[data.networkid] = user_data

        if SERVER then
            user_data["address"] = data.address
            ListAddress[data.address] = user_data
            sql.SaveValue("ip_tables", data.address, steamid64)
        end
    end
end


function onDisconnect(data)
    local user_data = getData(data.userid)
    user_data["entity"] = nil
    if user_data.index then
        ListEntIndex[user_data.index] = nil
    end
end

function onSpawn(data)
    local user_data = getData(data.userid)
    if SERVER then
        local ent = Entity(user_data.index)

        if not IsValid(ent) then error("Ent not is valid") end

        if not IsValid(user_data["entity"]) then
            hook.Run("OnPlayerInitialize", ent)
        end
        user_data["entity"] = ent
    end
end

if CLIENT then
    hook.Add("NetworkEntityCreated", "sourcedev.player.spawn", function(ent)
        if ent:IsPlayer() then
            local user_data = getData(ent:UserID())
            user_data["entity"] = ent
            hook.Run("OnPlayerInitialize", ent)
        end
    end)
end

function onChangeName(data)
    local user_data = getData(data.userid)

    user_data["name"] = data.newname

    if not user_data["old_name"] then user_data["old_name"] = {} end
    table.insert(user_data["old_name"], data.oldname)
end


if SERVER then
    gameevent.Listen( "player_connect" ) -- server
    hook.Add("player_connect", "sourcedev.player.connect", onConnect)
elseif CLIENT then
    gameevent.Listen( "player_connect_client" ) -- server + client
    hook.Add("player_connect_client", "sourcedev.player.connect", onConnect)
end

gameevent.Listen( "player_info" ) -- server + client
hook.Add("player_info", "sourcedev.player.connect", onConnect)


gameevent.Listen( "player_disconnect" ) -- server + client
hook.Add("player_disconnect", "sourcedev.player.disconnect", onDisconnect)

gameevent.Listen( "player_changename" ) -- server + client
hook.Add("player_changename", onChangeName)

gameevent.Listen( "player_spawn" ) -- server + client
hook.Add("player_spawn", onSpawn)

function GetBySteamID(sid)
    local data = ListSteamID[sid]
    if not data then return end

    local ent = data.entity
    return ent, data
end

GetBySteamID3 = GetBySteamID
GetBySteamID32 = GetBySteamID
GetBySteamID64 = GetBySteamID

function GetByID(id)
    local data = ListEntIndex[id]
    if not data then return end

    local ent = data.entity
    return ent, data
end



function Get(text)
    if not text then return end
    if ListSteamID[text] then return ListSteamID[text].entity, ListSteamID[text] end
    if ListAddress[text] then return ListAddress[text].entity, ListAddress[text] end
    if ListName[text] then return ListName[text].entity, ListName[text] end

    if text[1] == "#" then
        local text = tonumber(text:sub(2))
        if text and ListUserID[text] then return ListUserID[text].entity, ListUserID[text] end
    end

    if text[1] == "$" then
        local text = tonumber(text:sub(2))
        if text and ListEntIndex[text] then return ListEntIndex[text].entity, ListEntIndex[text] end
    end
end


function Find(text, who)
    local ply
    
    if not text or text == "" then return end

    if IsValid(who) then
        if text == "^" then
            ply = who

            goto result
        elseif text == "@" then
            local trace = who:GetEyeTraceHull(999, 10)
            if IsValid(trace.Entity) and trace.Entity:IsPlayer() then
                ply = trace.Entity
            end

            goto result
        elseif text == "@near" then
            local plys = {}


            local who_pos = who:GetPos()
            for k, ent in pairs(ents.FindInSphere(who:GetPos(), 300)) do
                if ent == who then continue end
                if IsValid(ent) and ent:IsPlayer() then
    
                    local dist = ent:GetPos():DistToSqr(who_pos)
                    plys[dist] = ent
                end
            end

            local _, fply = next(plys)
            ply = fply

            goto result
        end
    end


    if ListEntIndex[text] and ListEntIndex[text]["entity"] then ply = ListEntIndex[text]["entity"] goto result end
    if ListSteamID[text] and ListSteamID[text]["entity"] then ply = ListSteamID[text]["entity"] goto result end
    if ListAddress[text] and ListAddress[text]["entity"] then ply = ListAddress[text]["entity"] goto result end
    if ListName[text] and ListName[text]["entity"] then ply = ListName[text]["entity"] goto result end

    if text[1] == "#" then
        local text = tonumber(text:sub(2))
        if text and ListUserID[text] and ListUserID[text]["entity"] then ply = ListUserID[text]["entity"] end
        goto result
    end

    if text[1] == "$" then
        local text = tonumber(text:sub(2))
        if text and ListEntIndex[text] and ListEntIndex[text]["entity"] then ply = ListEntIndex[text]["entity"] end
        goto result
    end

    do
        local text = text:utf8lower()
        local fplys_find = {}
        for k, fply in ipairs(player.GetAll()) do
            local fply_name = fply:GetName():utf8lower()
            if fply_name == text then
                ply = fply
                break
            end
            local sp, ed = fply_name:find(text, 1, true)
            if sp then
                fplys_find[fply:UserID()] = fply
            end
        end


        local fid, fply = next(fplys_find)
        if next(fplys_find, fid) then
            if IsValid(who) and who:IsPlayer() then
                who:_chat( table.ToString(fplys_find, "Выберите игрока: ", true) )
                goto result
            end
        else
            ply = fply
            goto result
        end
    end

    ::result::

    return ply
end