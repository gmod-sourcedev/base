sourcedev("access")

function CanTool(ply, ent, tool)
    if CPPI and ent.CPPICanTool then return ent:CPPICanTool(ply, tool) end

    return ply:IsSuperAdmin()
end

function m_Entity:SDCanTool(ent, tool) return CanTool(self, ent, tool) end