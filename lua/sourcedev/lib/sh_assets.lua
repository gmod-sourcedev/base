sourcedev("assets")

if not file.IsDir("sourcedev", "DATA") then file.CreateDir("sourcedev") end
if not file.IsDir("sourcedev/assets", "DATA") then file.CreateDir("sourcedev/assets") end
if not file.IsDir("sourcedev/assets/materials", "DATA") then file.CreateDir("sourcedev/assets/materials") end
if not file.IsDir("sourcedev/assets/sound", "DATA") then file.CreateDir("sourcedev/assets/sound") end

function SaveURL(url, path, callback)
    http.Fetch(url, function(body, size, headers, code)
        local new_path = "data/sourcedev/assets/" .. path
        file.Write("sourcedev/assets/" .. path, body)
        if callback then callback(new_path) end
    end)
end

function Exists(path)
    return file.Exists("sourcedev/assets/" .. path, "DATA")
end

function Get(path)
    return file.Read("sourcedev/assets/" .. path, "DATA")
end

function Path(path)
    return "data/sourcedev/assets/" .. path
end


function Remove(path)
    return file.Delete("sourcedev/assets/" .. path)
end

function _G.MaterialURL(name, url, params)
    local path_url = "materials/" .. name
    params = params or "smooth mips"

    local return_mat
    if not Exists(path_url) then 
        return_mat = mat_copy["models/wireframe"]
        SaveURL(url, path_url, function(new_path)
            local new_mat = Material(new_path, params)
            return_mat:SetTexture("$basetexture", new_mat:GetTexture("$basetexture"))
        end)
    else
        return_mat = Material(Path(path_url), params)
    end

    return return_mat
end