sourcedev("sql")

-- do return end
SQLT_STRING = 1
SQLT_NUMBER = 2
SQLT_BOOL = 3
SQLT_VECTOR = 4
SQLT_TABLE = 5

ReadType = {
   [SQLT_STRING] = function(var) return var end,
   [SQLT_NUMBER] = function(var) return tonumber(var) end,
   [SQLT_BOOL] = function(var) return tobool(var) end,
   [SQLT_VECTOR] = function(var) return Vector(var) end,
   [SQLT_TABLE] = function(var) return util.JSONToTable(var) end,
}

WriteType = {
    ["string"] = function(var) return var,SQLT_STRING end,
    ["number"] = function(var) return tostring(var),SQLT_NUMBER end,
    ["boolean"] = function(var) return var and "1" or "0",SQLT_BOOL end,
    ["Vector"] = function(var) return tostring(var),SQLT_VECTOR end,
    ["table"] = function(var) return util.TableToJSON(var),SQLT_TABLE end,
}

do
    local res = sql.Query([[CREATE TABLE IF NOT EXISTS ss_data
    (
        `id` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        `type` INTEGER,
        `name` VARCHAR(255) NOT NULL,
        `key` VARCHAR(255) NOT NULL,
        `value` VARCHAR(255) NOT NULL,
        `tag` VARCHAR(255),
        UNIQUE(`name`, `key`)
    );]])
    if res == false then print(sql.LastError()) end
end

function SaveValue(name,key,value,tag)
    if not name then error("Name not exists: " .. tostring(name)) end
    if not key then error("Key not exists: " .. tostring(key)) end

    if value != nil then
        local tp = type(value)
        local func = WriteType[tp]

        if not func then error("Type not support: " .. tp) end

        local new_value, id_type = func(value)

        if not new_value or not id_type then
            error("Type result not exists: " .. tostring(value) .. " - " .. type(value))
        end

        local qe = tag and
            ( Format("REPLACE INTO ss_data(`type`, `name`, `key`, `value`, `tag`) VALUES('%s','%s','%s','%s', '%s');",id_type,name,key,new_value,tag) )
            or
            ( Format("REPLACE INTO ss_data(`type`, `name`, `key`, `value`) VALUES('%s','%s','%s','%s');",id_type,name,key,new_value) )
        local res = sql.Query( qe )
        if res == false then
            print(qe)
            error("SQL Error: " .. sql.LastError())
        end
    else
        local qe = tag and
            ( Format("DELETE FROM ss_data WHERE `name` = '%s' AND `key` = '%s' AND `tag` = '%s';",name,key,tag) )
            or
            ( Format("DELETE FROM ss_data WHERE `name` = '%s' AND `key` = '%s';",name,key) )
        local res = sql.Query( qe )
        if res == false then
            print(qe)
            error("SQL Error: " .. sql.LastError())
        end
    end
end

function ReadValue(name,key,tag)
    if not name then error("Name not exists: " .. tostring(name)) end
    if not key then error("Key not exists: " .. tostring(key)) end

    
    local qe = tag and
        ( Format("SELECT * FROM ss_data WHERE `name` = '%s' AND `key` = '%s' AND `tag` = '%s' LIMIT 1;",name,key,tag) )
        or
        ( Format("SELECT * FROM ss_data WHERE `name` = '%s' AND `key` = '%s' LIMIT 1;",name,key) )
    local res = sql.Query( qe )
    if res == false then
        print(qe)
        error("SQL Error: " .. sql.LastError())
    elseif res == nil then
        return nil
    end

    local data = res[1]
    local type_id = tonumber(data.type)
    local func = ReadType[type_id]
    if not func then error("Type not support: " .. type_id) end

    return func(data.value)
end

function BuildData(res, qe)
    if res == false then
        print(qe)
        error("SQL Error: " .. sql.LastError())
        return
    end

    if not res then return end

    local tbl = {}
    for k, data in pairs(res) do
        local type_id = tonumber(data.type)
        local func = ReadType[type_id]
        if not func then error("Type not support: " .. type_id) end
        tbl[data.key] = func(data.value)
    end

    return tbl
end

/*
    Reading
*/

function QueryErrorLogFormat(qe, ...)
    local qe = Format(qe, ...)
    local res = sql.Query(qe)

    if res == false then
        print(qe)
        error("SQL Error: " .. sql.LastError())
    end
    
    return res, qe
end

function ReadTableKey(var)
    assert(var,"Key not exists: " .. tostring(var))

    local res, qe = QueryErrorLogFormat("SELECT * FROM ss_data WHERE `key` = '%s';", var)

    return BuildData(res, qe)
end

function ReadTableName(name)
    assert(name,"Name not exists: " .. tostring(name))

    local res, qe = QueryErrorLogFormat("SELECT * FROM ss_data WHERE `name` = '%s';",name)
    
    return BuildData(res, qe)
end

function ReadTableTag(tag)
    assert(tag,"Tag not exists: " .. tostring(tag))

    local res, qe = QueryErrorLogFormat("SELECT * FROM ss_data WHERE `tag` = '%s';",tag)
    
    return BuildData(res, qe)
end

function ReadTableNameKey(name, key)
    assert(name,"Name not exists: " .. tostring(name))
    assert(key,"Key not exists: " .. tostring(key))

    local res, qe = QueryErrorLogFormat("SELECT * FROM ss_data WHERE `name` = '%s' AND `key` = '%s';",name, key)
    
    return BuildData(res, qe)
end

function ReadTableNameTag(name, tag)
    assert(name,"Name not exists: " .. tostring(name))
    assert(tag,"Tag not exists: " .. tostring(tag))

    local res, qe = QueryErrorLogFormat("SELECT * FROM ss_data WHERE `name` = '%s' AND `tag` = '%s';",name, tag)
    
    return BuildData(res, qe)
end

/*
    Clearing
*/

function ClearTableValue(var)
    assert(var, "Value not exists: " .. tostring(var))

    QueryErrorLogFormat("DELETE FROM ss_data WHERE `value` = '%s';", var)
end

function ClearTableName(var)
    assert(var, "Name not exists: " .. tostring(var))

    QueryErrorLogFormat("DELETE FROM ss_data WHERE `name` = '%s';", var)
end

function ClearTableKey(var)
    assert(var, "Key not exists: " .. tostring(var))

    QueryErrorLogFormat("DELETE FROM ss_data WHERE `key` = '%s';", var)
end

function ClearTableTag(var)
    assert(var, "Tag not exists: " .. tostring(var))

    QueryErrorLogFormat("DELETE FROM ss_data WHERE `tag` = '%s';", var)
end


function ClearTableNameTag(var1, var2)
    assert(var1, "Name not exists: " .. tostring(var1))
    assert(var2, "Tag not exists: " .. tostring(var2))

    QueryErrorLogFormat("DELETE FROM ss_data WHERE `name` = '%s' AND `tag` = '%s';", var1, var2)
end

function ClearTableNameKey(var1, var2)
    assert(var1, "Name not exists: " .. tostring(var1))
    assert(var2, "Key not exists: " .. tostring(var2))

    QueryErrorLogFormat("DELETE FROM ss_data WHERE `name` = '%s' AND `key` = '%s';", var1, var2)
end

function ClearTableKeyTag(var1, var2)
    assert(var1, "Key not exists: " .. tostring(var1))
    assert(var2, "Tag not exists: " .. tostring(var2))

    QueryErrorLogFormat("DELETE FROM ss_data WHERE `key` = '%s' AND `tag` = '%s';", var1, var2)
end

function ClearTableNameKeyTag(var1, var2, var3)
    assert(var1, "Name not exists: " .. tostring(var1))
    assert(var2, "Key not exists: " .. tostring(var2))
    assert(var3, "Tag not exists: " .. tostring(var3))

    QueryErrorLogFormat("DELETE FROM ss_data WHERE `name` = '%s' AND `key` = '%s' AND `tag` = '%s';", var1, var2, var3)
end

-- local PLAYER = FindMetaTable("Player")


-- function PLAYER:SetPData(key, var, tag)
--     SaveValue(self:SteamID64(), key, var, tag)
-- end


-- function PLAYER:PData(key, def)
--     return ReadValue(self:SteamID64(), key) or def
-- end
-- PLAYER.GetPData = PLAYER.PData
-- function PLAYER:RemovePData(key) self:SetPData(key, nil) end

-- function PLAYER:PDataTag(key, tag, def)
--     return ReadValue(self:SteamID64(), key, tag) or def
-- end

-- print("testsql")