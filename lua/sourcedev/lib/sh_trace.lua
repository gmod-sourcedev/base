m_Entity = FindMetaTable("Entity")

function m_Entity:GetEyeTraceHull(distance, hull)
    distance = distance or 15
    hull = hull or 15
	local dir = self:GetAimVector()

	local trace = {}

	trace.start = self:EyePos()
	trace.endpos = trace.start + (dir * distance)
	trace.filter = self
    trace.mins = Vector(-hull,-hull,-hull)
    trace.maxs = Vector(hull,hull,hull)
    trace.mask = MASK_ALL

    return util.TraceHull(trace)
end

function m_Entity:GetEyeTraceNoCursorHull(distance, hull)
    distance = distance or 15
    hull = hull or 15
	local dir = self:EyeAngles():Forward()

	local trace = {}

	trace.start = self:EyePos()
	trace.endpos = trace.start + (dir * distance)
	trace.filter = self
    trace.mins = Vector(-hull,-hull,-hull)
    trace.maxs = Vector(hull,hull,hull)
    trace.mask = MASK_ALL

    return util.TraceHull(trace)
end

function m_Entity:GetEyeTraceRay(distance, hull)
    distance = distance or 15
    hull = hull or 15
	local dir = self:GetAimVector()

	local start = self:EyePos()
	local endpos = start + ( dir * distance )
    local mins = Vector(-hull,-hull,-hull)
    local maxs = Vector(hull,hull,hull)
    
    return ents.FindAlongRay(start, endpos, mins, maxs)
end


function m_Entity:GetEyeTraceRayNoCursorHull(distance, hull)
    distance = distance or 15
    hull = hull or 15
	local dir = self:EyeAngles():Forward()

	local start = self:EyePos()
	local endpos = start + ( dir * distance )
    local mins = Vector(-hull,-hull,-hull)
    local maxs = Vector(hull,hull,hull)
    
    return ents.FindAlongRay(start, endpos, mins, maxs)
end