sourcedev("pdata")

function read(steamid64, key)
    return sql.ReadValue(steamid64, key, "sourcedev_player_save")
end

function m_Player:_read(key)
    return sql.ReadValue(self:SteamID64(), key, "sourcedev_player_save")
end

function save(steamid64, key, value)
    return sql.SaveValue(steamid64, key, value, "sourcedev_player_save")
end

function m_Player:_save(key, value)
    sql.SaveValue(self:SteamID64(), key, value, "sourcedev_player_save")
    self:_notify("Данные обновлены: " .. tostring(key) .. " = " .. tostring(value))
end

function m_Player:_perm(perm, target)
    if self:IsSuperAdmin() then return true end
    if not perm then return false end

    local result = sql.ReadValue(self:SteamID64(), perm, "sourcedev_access")
    if result then return result end

    return nil
end

function m_Player:_permAll()
    local result = sql.ReadTableNameTag(self:SteamID64(), "sourcedev_access")
    if result then return result end

    return nil
end

MODULE.CachedPerm = MODULE.CachedPerm or {}

function m_Player:_permCached(perm, target)
    if self:IsSuperAdmin() then return true end
    if not perm then return false end

    ::check::
    if CachedPerm[self] and CachedPerm[self][perm] != nil then return CachedPerm[self][perm] end

    CachedPerm[self] = CachedPerm[self] or {}

    do
        local result = self:_perm(perm, target)
        if result == nil then
            CachedPerm[self][perm] = false
            goto check
        else
            CachedPerm[self][perm] = perm
            goto check
        end
    end

    return nil
end


function m_Player:_setPerm(perm, value)
    if not perm then return false end

    sql.SaveValue(self:SteamID64(), perm, value, "sourcedev_access")

    if not CachedPerm[self] then CachedPerm[self] = {} end
    CachedPerm[self][perm] = value
 
    if value then 
        self:_notify("Вы получили доступ к: " .. perm .. " = " .. tostring(value), NOTIFY_SUCCESS, 10)
    else
        self:_notify("Вы потеряли доступ к: " .. perm, NOTIFY_ERROR, 10)
    end

    return false
end

function m_Player:_clearPerm()
    
    CachedPerm[self] = nil
    sql.ClearTableNameTag(self:SteamID64(), "sourcedev_access")

    self:_notify("У вас забрали весь доступ", NOTIFY_ERROR, 10)
end