sourcedev("restrictions")

function m_Player:_lock(time)
    self:SetNWBool("sourcedev.lock", true)

    if time then
        self:__timerSimple(time, function()
            self:_unLock()
        end)
    end
end

function m_Player:_unLock()
    self:SetNWBool("sourcedev.lock", false)
end

function m_Player:_isLocked()
    return self:GetNWBool("sourcedev.lock")
end

hook.Add("StartCommand", "sourcedev.player_lock", function(ply, cmd)
    if ply:_isLocked() then
        cmd:ClearButtons()
        cmd:ClearMovement()
    end
end)

if CLIENT then
    local clr_red = clr.red:Copy()
    hook.Add("HUDPaint", "sourcedev.player_lock", function()
        local ply = LocalPlayer()

        if ply:_isLocked() then
            ui.TextShadow("Блокировка управления", 10, 990, 990, clr_red, 2, 4)
        end
    end)
end



