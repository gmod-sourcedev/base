sourcedev()

-- do return end
m_Entity = FindMetaTable("Entity")


function m_Player:__info()
    return table.concat({self:GetName(),"(",(self:SteamID64() or "BOT"),")"}, "")
end

function m_Entity:__cooldown(name, add)
    add = add or 1
    if not self.__CoolDowns then self.__CoolDowns = {} end

    if self.__CoolDowns[name] and self.__CoolDowns[name] > CurTime() then
        return true, math.Round(self.__CoolDowns[name] - CurTime(),2)
    end

    self.__CoolDowns[name] = CurTime() + add
end

function m_Entity:__spend(name, clear)
    if clear and self.__Used and self.__Used[name] then self.__Used[name] = nil return end

    if not self.__Used then
        self.__Used = { [name] = true }
        return
    end

    if not self.__Used[name] then
        self.__Used[name] = true
        return
    end

    return true
end

function m_Entity:_debugEnabled()
    return self:KeyDown(IN_ALT2) and self:_permCached("debug")
end

function m_Entity:__timerSimple(time, func, funcNoValid, ...)
    local args = {...}
    timer.Simple(time, function()
        if IsValid(self) then
            if func then func(self, unpack(args)) end
        elseif funcNoValid then
            funcNoValid(self, unpack(args))
        end
    end)
end

function m_Entity:__timerName(name)
    return table.concat({"ENT_TIMER",ent:EntIndex(),name}, "_")
end

function m_Entity:__timerCreate(name, cooldown, reps, func, funcNoValid, ...)
    local args = {...}
    local timer_name = self:__timerName(name)
    timer.Create(timer_name, cooldown, reps, function()
        if IsValid(self) then
            if func then func(self, unpack(args)) end
        elseif funcNoValid then
            funcNoValid(self, unpack(args))
        end
    end)
end

function m_Entity:__timerRemove(name)
    local timer_name = self:__timerName(name)
    timer.Remove(timer_name)
end


function m_Entity:__give(name, amount)
    amount = amount or 1

    if not self.__Has then
        self.__Has = { [name] = amount }
        return
    end

    if not self.__Has[name] then
        self.__Has[name] = amount
        return
    end

    self.__Has[name] = self.__Has[name] + amount

    return true
end


function m_Entity:__has(name)
    if not self.__Has then return false end

    if not self.__Has[name] then return false end

    return self.__Has[name] > 0
end

function m_Entity:__take(name, wait)
    if not self.__Has then return false end

    if not self.__Has[name] then return false end

    self.__Has[name] = self.__Has[name] - 1
    
    if not wait then
        if self.__Has[name] <= 0 then
            self.__Has[name] = nil
            if table.IsEmpty(name) then
                self.__Has = nil
            end
        end
    else
        self:__timerSimple(wait, function(self)
            self.__take(name)
        end)
    end
    
    return true
end


function string:IsSteamID32(str)
	return self:match('^STEAM_%d:%d:%d+$')
end

function string:IsSteamID64()
	return (self:len() == 17) and (self:sub(1, 4) == '7656')
end

function m_Entity:GetModelBoundsFixed()
	local min, max = self:GetModelBounds()
	local minn, maxx = Vector(min), Vector(max)
    if min.x != -max.x then
		local md_x = max.x - min.x
		minn.x = -md_x/2
		maxx.x = md_x/2
	end
	if min.y != -max.y then
		local md_y = max.y - min.y
		minn.y = -md_y/2
		maxx.y = md_y/2
	end
	if min.z != -max.z then
		local md_z = max.z - min.z
		minn.z = -md_z/2
		maxx.z = md_z/2
	end
    return minn, maxx
end