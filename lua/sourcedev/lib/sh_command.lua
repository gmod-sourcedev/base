sourcedev("cmd")

if SERVER then
    util.AddNetworkString("sourcedev.cmd.notify")
    util.AddNetworkString("sourcedev.cmd.chat")
    function notify(plys, content, tp, len)
        tp = tp or NOTIFY_HINT
        len = len or 4
        
        if not content then error("not content") end
        net.Start("sourcedev.cmd.notify")
            net.WriteUInt(tp,4)
            net.WriteUInt(len, 6)
            net.WriteString(content)
        if IsValid(plys) then net.Send(plys) else net.Broadcast() end
    end

    function chat(plys, ...)
        local args = {...}

        net.Start("sourcedev.cmd.chat")
            net.WriteTable(args)
        if IsValid(plys) then net.Send(plys) else net.Broadcast() end
    end
    m_Player._notify = notify
    m_Player._chat = chat

    MODULE.ListPlayers = MODULE.ListPlayers or {}
    hook.Add("PlayerInitialSpawn", "sourcedev.cmd", function(ply)
        local steamid = ply:SteamID64(); if isstring(steamid) then ListPlayers[steamid] = ply end
        local steamid32 = ply:SteamID(); if isstring(steamid32) then ListPlayers[steamid32] = ply end
        local accountid = ply:AccountID(); if isstring(accountid) then ListPlayers[accountid] = ply end
        local address = ply:IPAddress(); if isstring(address) then ListPlayers[address] = ply end
        local userid = ply:UserID(); if isstring(userid) then ListPlayers["#" .. userid] = ply end
    end)

    hook.Add("PlayerDisconnected", "sourcedev.cmd", function(ply)
        local steamid = ply:SteamID64(); if isstring(steamid) then ListPlayers[steamid] = nil end
        local steamid32 = ply:SteamID(); if isstring(steamid32) then ListPlayers[steamid32] = nil end
        local accountid = ply:AccountID(); if isstring(accountid) then ListPlayers[accountid] = nil end
        local address = ply:IPAddress(); if isstring(address) then ListPlayers[address] = nil end
        local userid = ply:UserID(); if isstring(userid) then ListPlayers["#" .. userid] = nil end
    end)

    function findPlayer(text, who)
        local ply

        if not text then return end
        if text == "" then return end
    
        if text == "^" then
            if IsValid(who) and who:IsPlayer() then ply = who end

            goto result
        end

        if text == "@" then
            if IsValid(who) and who:IsPlayer() then 
                local trace = who:GetEyeTraceHull(999, 10)
                if IsValid(trace.Entity) and trace.Entity:IsPlayer() then
                    ply = trace.Entity
                end
            end

            goto result
        end


        if text == "@near" then
            if IsValid(who) and who:IsPlayer() then
                local plys = {}

                for k, ent in pairs(ents.FindInSphere(who:GetPos(), 300)) do
                    if ent == who then continue end
                    if IsValid(trace.Entity) and trace.Entity:IsPlayer() then
                        -- ply = trace.Entity
                        table.insert(plys, trace.Entity)
                    end
                end

                local who_pos = who:GetPos()
                table.sort( plys, function(a, b) return a:GetPos():DistToSqr(who_pos) > b:GetPos():DistToSqr(who_pos) end )

                ply = next(plys)
            end

            goto result
        end

        if ListPlayers[text] then ply = ListPlayers[text] goto result end

        do
            local text = text:lower()
            local fplys_find = {}
            for k, fply in ipairs(player.GetAll()) do
                local fply_name = fply:GetName():lower()
                if fply_name == text then
                    ply = fply
                    break
                end
                local sp, ed = fply_name:find(text, 1, true)
                if sp then
                    fplys_find[fply:UserID()] = fply
                end
            end


            local fid, fply = next(fplys_find)
            if next(fplys_find, fid) then
                if IsValid(who) and who:IsPlayer() then
                    who:_chat( table.ToString(fplys_find, "Выберите игрока: ", true) )
                    goto result
                end
            else
                ply = fply
                goto result
            end
        end

        ::result::

        return ply
    end


elseif CLIENT then
    function notify(plys, content, tp, len)
        tp = tp or NOTIFY_HINT
        len = len or 4

        notification.AddLegacy(content, tp, len)
    end

    function chat(plys, ...)
        chat.AddText(...)
    end

    net.Receive("sourcedev.cmd.notify", function()
        local tp = net.ReadUInt(4)
        local len = net.ReadUInt(6)
        local content = net.ReadString()
        notification.AddLegacy(content, tp, len)
    end)
    net.Receive("sourcedev.cmd.chat", function()
        local content = net.ReadTable()
        _G.chat.AddText(unpack(content))
        MsgC(unpack(content))
        MsgC("\n")
    end)

    m_Player._notify = notify
    m_Player._chat = chat
end


MODULE.List = MODULE.List or {}


function getStringArgs(text, removefirst)
    local marks = {}

    text = string.gsub(text, '(\"([^"]+)\")', function(mark_str, mark_content)
        table.insert(marks, mark_content)
        
        return "\1"
    end)

    local args = string.Split(text, " ")
    for k, v in ipairs(args) do
        if v == "\1" then
            args[k] = marks[1]
            table.remove(marks, 1)
        end
    end

    if removefirst then
        table.remove(args, 1)
    end

    return args
end



function onCommand(ply, cmd, args, argsString)
    if not IsValid(ply) then ply = nil end
    cmd = string.lower(cmd)

    local is_console = not ply

    if cmd[1] == "." then cmd = cmd:sub(2) end

    local cmd_data = List[cmd]
    if not cmd_data then 
        if is_console then 
            MsgC(clr.red, "[SR] ", clr.white, "Command not founded!","\n") 
        else 
            ply:_notify("Command not founded", NOTIFY_ERROR) 
        end
        return
    end

    local func = cmd_data.func

    local res, comm = func(ply, cmd, args, argsString)
    if isstring(comm) then
        local tp = NOTIFY_HINT
        if res == true then tp = NOTIFY_SUCCESS end
        if res == false then tp = NOTIFY_ERROR end
        if is_console then
            if comm:sub(1, 5) == "CHAT " then comm = comm:sub(6) end
            MsgC(clr.red, "[SR] ", clr.white, comm,"\n") 
        else 
            if comm:sub(1, 5) == "CHAT " then
                ply:_chat(clr.red, "[SR] ", clr.white, comm:sub(6))
            else
                ply:_notify(comm, tp) 
            end
        end
    elseif istable(comm) then
        if is_console then
            MsgC(clr.red, "[SR] ", clr.white)
            MsgC(unpack(comm))
            MsgC("\n") 
        else 
            ply:_chat(clr.red, "[SR] ", clr.white, unpack(comm))
        end
    else
        if is_console then MsgC(clr.red, "[SR] ", clr.white, "Command Executed","\n") else ply:_notify("Команда исполнена!") end
    end
end

function regCommand(cmd, func, data)
    cmd = string.lower(cmd)

    List[cmd] = {
        func = func,
        data = data
    }

    concommand.Add("." .. cmd, function(ply, cmd, _, argsS )
        local args = getStringArgs(argsS)
        
        onCommand(ply, cmd, args, argsS)
    end)
end

function readValue(value)
    local num_value = tonumber(value)
    if num_value then
        if num_value == 0 then
            value = false
        else
            value = num_value
        end
    else
        if value == "true" then
            value = true
        elseif value == "false" then
            value = false
        elseif value == "nil" then
            value = nil
        end
    end

    return value
end

if SERVER then
    hook.Add("PlayerSay", "sourcedev.cmd", function(ply, text)
        if text[1] == "." then
            if ply:__cooldown("sourcedev.cmd") then ply:_notify("Не так часто!", NOTIFY_ERROR) return "" end
            local args = getStringArgs(text)
            local cmd = args[1]
            table.remove(args, 1)

            local argsString = table.concat(args, " ")

            onCommand(ply, cmd, args, argsString)

            return ""
        end
    end)
end



if SERVER then
    local help = function(ply, cmd, args, argsString)
        local text = [[Aviable commands: ]]

        local points = table.concat(table.GetKeys(List), ",")

        return true, "CHAT " .. text .. points
    end
    
    regCommand("?", help)
    regCommand("help", help)
    regCommand("commands", help)
end