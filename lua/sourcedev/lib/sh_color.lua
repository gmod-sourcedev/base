sourcedev("color")


colors = MODULE.colors or {}
debug.setmetatable(colors, {
    __index = function(self, key)
        return rawget(self, key)
    end,
    __newindex = function(self, key)
        return
    end
})

sourcedev.clr = colors
_G.clr= colors
function ColorSaved(r, g, b, a)
    local new_color = {r = r, g = g, b = b, a = a or 255}
    debug.setmetatable(new_color, {
        __index = function(self, key)
            return m_Color[key] or rawget(self, key)
        end,
        __newindex = function(self, key, value)
            local tbl = table.Copy(self)
            tbl.key = value
            return Color(tbl.r, tbl.g, tbl.b, tbl.a)
        end,
        __tostring = function(self)
            return string.format( "%d %d %d %d %s", self.r, self.g, self.b, self.a, "saved" )
        end,
    })
    return new_color
end

function SetupColor(key, r, g, b, a)
    rawset(colors,key, ColorSaved(r, g, b, a))
end

SetupColor("white", 255, 255, 255)
SetupColor("black", 0, 0, 0)

SetupColor("red", 255, 0, 0)
SetupColor("green", 0, 255, 0)
SetupColor("blue", 0, 0, 255)

SetupColor("fav", 255, 125, 255)
SetupColor("sky", 0, 125, 255)


function m_Color:Alpha(value)
    return ColorAlpha(self, value or 255)
end

function m_Color:Copy()
    return Color(self.r, self.g, self.b, self.a)
end
