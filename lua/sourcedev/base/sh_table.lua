sourcedev("base")


function MetaTable(meta)
    local meta = meta or {}

    return setmetatable({}, meta), meta
end
