if not file.IsDir("sourcedev", "DATA") then file.CreateDir("sourcedev") end
if not file.IsDir("sourcedev/debug", "DATA") then file.CreateDir("sourcedev/debug") end


local function arg_to_string(arg)
    if isentity(arg) then
        if arg:IsPlayer() then
            return arg:SteamID64() .. "|" .. arg:GetName()
        else
            return arg:GetClass() .. "|" .. arg:GetModel()
        end
    elseif istable(arg) then
        return util.TableToKeyValues(arg)
    else
        return tostring(arg)
    end
end


local meta_debug = {}


function meta_debug:log(...)
    local args = {...}

    MsgC(clr.red, '[', clr.white, 'SR', clr.red, ':d:', clr.white, self.name, clr.red, ']', clr.white, ': ')
    for k, v in ipairs(args) do
        MsgC(arg_to_string(v), ' ')
    end
    MsgC('\n')
    
    for k, v in ipairs(args) do
        self.fl:Write(arg_to_string(v) .. '\n')
    end

    self.fl:Flush()
end

function meta_debug:logTrace(...)
    local args = {...}

    self.fl:Write(debug.traceback() .. '\n')
    self:log(unpack(args))
end

meta_debug.__index = meta_debug


debug.mt_channels = debug.mt_channels or {}
function debug.getChannel(name)
    if not debug.mt_channels[name] then
    
        debug.mt_channels[name] = setmetatable({}, meta_debug)
        debug.mt_channels[name].name = name
        debug.mt_channels[name].fl = file.Open("sourcedev/debug/" .. name .. '.txt', "wb", "DATA")
    end

    return debug.mt_channels[name]
end

DEBUGCHL_MAIN = debug.getChannel("main")

DEBUGCHL_MAIN:logTrace(debug)