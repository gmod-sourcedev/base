sourcedev("stat_variable")

MODULE.List = MODULE.List or {}

MODULE.meta = MODULE.meta or {}

setmetatable(meta, {
    __index = function(self, key)
        local var = rawget(List, key)
        local tp = type(var)
        if tp == "function" then 
            return var()
        else
            return var
        end
    end,
    __newindex = function(self, key, value)
        rawset(List, key, value)
    end
})


_G.stat = MODULE.meta
_G.STAT = _G.stat