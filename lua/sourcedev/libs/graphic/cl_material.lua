sourcedev("graphics.materials")


List = MODULE.List or {}
ListCopied = MODULE.ListCopied or {}
_G.mat = base.MetaTable{
    __index = function(self, key)
        if List[key] then 
            return List[key]
        else
            List[key] = Material(key)

            if not List[key] or List[key]:IsError() then
                List[key] = Material("models/wireframe")
            end
        end

        return List[key]
    end,
    __newindex = function(self, key, value)
        List[key] = value
    end
}

_G.mat_copy, mat_copy_meta = base.MetaTable{
    __index = function(self, key)
        local IMat --= type(key) == ( "IMaterial" and key or Material(key) ) or mat[key]
        if type(key) == "IMaterial" then
            IMat = key
        elseif isstring(key) then
            IMat = Material(key)
        end

        if not IMat or IMat:IsError() then
            IMat = Material("models/wireframe")
        end
        
        return Copy(formatter.GetRandomWords(), IMat)
    end,
}

mat.wireframe = mat["models/wireframe"]
mat.base = mat["models/props_lab/cornerunit_cloud"]


function Copy(new_name, IMat)
    local name = IMat:GetName()
    local shaders = IMat:GetShader()
    local new_mat = CreateMaterial(new_name, shaders, {
        ["$basetexture"] = name
    })
    local newid = table.insert(ListCopied, new_mat)
    return new_mat, newid
end