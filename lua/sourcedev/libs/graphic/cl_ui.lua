sourcedev("graphics.ui")
sourcedev.ui = MODULE
_G.ui = MODULE


fonts = fonts or {}
function font(source, data, not_save)
	if fonts[source] then return fonts[source] end
	local font_name,font_size,add,addstr
	if isnumber(source) then
		font_name = "Roboto"
		font_base = "Roboto"
		font_size = source
	elseif isstring(source) then
		local prepare_name = source
		prepare_name = string.gsub(prepare_name, "([+|-])(%a+)(%d*)", function(bool, key, value)
			if not add then add = {} end
			if not addstr then addstr = "" end
			addstr = addstr .. bool .. key .. value
			bool = bool == "+" and true or false


			if value and value != "" then
				value = tonumber(value)
				if bool == false then value = -value end
				add[key] = value
			else
				add[key] = bool
			end

			return ""
		end)

		local sizen, fontn = string.match(prepare_name, "(%d+):(.*)")
		if sizen and fontn then
			font_size = tonumber(sizen) or 10
			font_base = fontn
		elseif not data and not add then
			fonts[source] = source
			return fonts[source]
		else
			font_size = 10
		end

		font_name = source
	end

	font_name = font_name or "Roboto"
	font_base = font_base or "Roboto"

	local font_name = "ui_font." .. font_size .. "." .. font_name	

	fonts[source] = font_name

	local font_data = {
		font = font_base,
		extended = false,
		size = font_size * ( ScrH() / 350.0 ),
		weight = 500,
		blursize = 0,
		scanlines = 0,
		antialias = true,
		underline = false,
		italic = false,
		strikeout = false,
		symbol = false,
		rotary = false,
		shadow = false,
		additive = false,
		outline = false,
	}

	if data then
		if not_save then
			font_data = data
		else
			table.Merge(font_data, data)
		end
	end

	if add then
		table.Merge(font_data, add)
	end

	surface.CreateFont(fonts[source], font_data)
	return fonts[source]
end

ui_weight,ui_height = ScrW()/1000,ScrH()/1000

_G.ui_font = font

hook.Add("OnScreenSizeChanged","UI.ChangeUISizes",function(w,h)
	ui_weight,ui_height = ScrW()/1000,ScrH()/1000
	fonts = {}
	update_screen_size()
end)


function MODULE.wide(x)
	-- if x < 0 then x = 1000 + x end
	return ui_weight * x
end

function MODULE.height(y)
	-- if y < 0 then y = 1000 + y end
	return ui_height * y
end

_GG.wide = setmetatable({}, {
	__index = function(self, key)
		return MODULE.wide(key)
	end,
	__add = function(self, key)
		return MODULE.wide(key)
	end,
	__sub = function(self, key)
		return MODULE.wide(-key)
	end,
	__call = function(self, key)
		return MODULE.wide(key)
	end,
	__mod = function(self, key, key2)
		return MODULE.wide(key*10)
	end
})


_GG.height = setmetatable({}, {
	__index = function(self, key)
		return MODULE.height(key)
	end,
	__add = function(self, key)
		return MODULE.height(key)
	end,
	__sub = function(self, key)
		return MODULE.height(-key)
	end,
	__call = function(self, key)
		return MODULE.height(key)
	end,
	__mod = function(self, key)
		return MODULE.height(key*10)
	end
})

_G.x, _G.y, _G.w, _G.h = _GG.wide,_GG.height,_GG.wide,_GG.height
_G._x, _G._y, _G._w, _G._h = _GG.wide,_GG.height,_GG.wide,_GG.height
_G.xx, _G.yy, _G.ww, _G.hh = _GG.wide,_GG.height,_GG.wide,_GG.height


-- multiscren function
do 
	function Box(x, y, w, h, color)
		surface.SetDrawColor(color.r, color.g, color.b, color.a)
		surface.DrawRect(xx[x], yy[y], ww[w], hh[h])
	end

	function BoxMaterial(x, y, w, h, color, mat)
		surface.SetMaterial(mat)
		surface.SetDrawColor(color.r, color.g, color.b, color.a)
		surface.DrawTexturedRect(xx[x], yy[y], ww[w], hh[h])
	end

	function BoxRound(round, x, y, w, h, color)
		draw.RoundedBox(yy[round], xx[x], yy[y], ww[w], hh[h], color)
	end

	function Text(text, fontn, x, y, color, alw, alh)
		draw.SimpleText(text, font(fontn), xx[x], yy[y], color or clr.white, alw or TEXT_ALIGN_LEFT, alh or TEXT_ALIGN_TOP)
	end
	local clr_black = clr.black:Copy()
	function TextShadow(text, fontn, x, y, color, alw, alh, color_bg)
		draw.SimpleText(text, font(fontn), xx[x]+1, yy[y]+1, color_bg or clr_black or clr.white, alw or TEXT_ALIGN_LEFT, alh or TEXT_ALIGN_TOP)
		draw.SimpleText(text, font(fontn), xx[x], yy[y], color or clr.white, alw or TEXT_ALIGN_LEFT, alh or TEXT_ALIGN_TOP)
	end
end

-- px function
do 
	function DrawPoly(poly,color)
		surface.SetDrawColor(color.r,color.g,color.b,color.a)
		surface.DrawPoly(poly)
	end
end

sourcedev("graphics.ui")
sourcedev.ui2 = MODULE
_G.ui2 = MODULE


do
	function Box(x, y, w, h, color)
		surface.SetDrawColor(color.r, color.g, color.b, color.a)
		surface.DrawRect(x, y, w, h)
	end

	function BoxMaterial(x, y, w, h, color, mat)
		surface.SetMaterial(mat)
		surface.SetDrawColor(color.r, color.g, color.b, color.a)
		surface.DrawTexturedRect(x, y, w, h)
	end

	function BoxMaterialRotate(x, y, w, h, color, mat, rot)
		surface.SetMaterial(mat)
		surface.SetDrawColor(color.r, color.g, color.b, color.a)
		surface.DrawTexturedRectRotated(x, y, w, h, rot)
	end



end


