sourcedev("graphics.effects")

hook.Remove("PostDrawEffects", "sourcedev.util.effects", function()
    cam.Start3D()
    hook.Run("Draw3D")
    cam.End3D()
end)


hook.Remove("Draw3D", "DrawThis", function()
    local this = find.this
    if this then
        hook.Run("Draw3DThis", this)
    end
end)