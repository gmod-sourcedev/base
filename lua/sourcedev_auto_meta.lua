sourcedev = sourcedev or {}
local meta = sourcedev

meta.CopyVar = function(self, key, value)
    rawset(self, key, value)
    return value
end
meta.CopyTable = function(self, tbl)
    local copied = {}
    if not tbl then return end
    for k, v in pairs(tbl) do
        if copied[k] then continue end
        copied[k] = true
        if istable(v) then
            if not copied[v] then continue end
            copied[v] = true

            local new_tbl = meta.CopyVar(self, k, {})
            meta.CopyTable(new_tbl, v)
            continue
        end

        meta.CopyVar(self, k, v)
    end
    return self
end
meta.newmodule = function(self, name)
    if self.LL[name] then return self.LL[name] end

    self.LL[name] = {}
    local MOD = rawget(self.LL,name)

    local LG = rawget(self._LG, name)
    if not istable(LG) then LG = nil end

    MOD.L = MOD.L or LG or {}
    MOD._LG = LG or self.LG or {}
    MOD.L.L = MOD.L or {}

    MOD.L.MODULE = MOD.L

    -- MOD.L.newmodule = meta._meta.newmodule


    MOD.LL = MOD.LL or {}

    self.L[name] = MOD.L
    return setmetatable(MOD, meta._meta)
end
meta.L = meta.L or {}
meta.L.L = meta.L

meta.L.meta = meta
meta.L.debug = debug
meta.L._G = _G
meta.L._GG = _G
meta.L._LG = _G


meta.LL = meta.LL or {}

meta._G = meta._G or meta.CopyTable({}, _G)
meta._GG = _G
meta._meta = {
    __index = function(self, key)
        if not rawget(rawget(self, "L"), key) then
            if meta.L[key] then
                return meta.L[key]
            elseif rawget(meta._G,key) or rawget(_G, key) then
                meta.L[key] = meta._G[key] or _G[key]
                return meta.L[key]
            end
            -- elseif rawget(rawget(self,L),key)
        end

        return rawget(rawget(self, "L"), key)
    end,
    __newindex = function(self, key, value)
        if value then
            local tp = type(value)
            if tp == "string" then
                local start = value[1]
                if start == ":" then
                    value = value
                elseif value == ">" then
                    value = CompileString(value:sub(2), "")
                end
            end
        end
        self.L[key] = value
    end,
    __tostring = function(self, key, value)
        return "gmod-meta"
    end,
    __call = function(self, module_name)
        if module_name == "meta" then return setfenv(1, meta) end
        if module_name == "_G" then return setfenv(1, _G) end

        if module_name then
            local list_modules
            if module_name:find(".") then
                list_modules = string.Split(module_name, ".")
            else
                list_modules = {module_name}
            end


            local last_module = meta
            for _, module_name in ipairs(list_modules) do
                last_module = rawget(meta, "newmodule")(last_module, module_name)
            end
            return setfenv(1, last_module)
        else
            return setfenv(1, meta)
        end
    end,
    __metatable = function(self)
        return nil
    end,
}
debug.setmetatable(meta, meta._meta)