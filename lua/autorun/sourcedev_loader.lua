if not sourcedev then
    AddCSLuaFile("sourcedev_auto_meta.lua")
    include("sourcedev_auto_meta.lua")
    print("[source_dev] your_server_has dash! IT's BAD")
end

sourcedev = sourcedev or {}


sourcedev()

function include_fl(path)
    local russ, err = pcall(include, path)
    if ress == false then print("sourcedev include error: \n - " .. path .. "\n" .. err) end
end

function load_fl(path)
    local fl_name = string.GetFileFromFilename(path)
    local ext = string.GetExtensionFromFilename(path)

    if ext == "lua" then
        if fl_name:sub(1, 3) == "cl_" then
            AddCSLuaFile(path)
            if CLIENT then include_fl(path) end
        elseif fl_name:sub(1, 3) == "sh_" then
            AddCSLuaFile(path)
            include_fl(path)
        elseif fl_name:sub(1, 3) == "sv_" then
            if SERVER then include_fl(path) end
        end
    end
end

local rules_folder = {
    base = 0,
    lib = 1,
    libs = 2,

    core = 3,
    modules = 4,
    vgui = 5,
}

function sortFolders(tbl)
    table.sort(tbl, function(a, b)
        if rules_folder[a] and rules_folder[b] then
            return rules_folder[a] < rules_folder[b]
        elseif rules_folder[a] and not rules_folder[b] then
            return true
        elseif not rules_folder[a] and rules_folder[b] then
            return false
        end

        return a < b
    end)
end

function load_folder(path, sub_folder_lvl)
    local files, folders = file.Find(path .. "/*", "LUA")
    if not files and not folders then error("Path not found!") end


    for k, v in pairs(files) do
        local res, err = pcall(load_fl, path .. "/" .. v)
        if res == false then print("sourcedev include error:\n" .. err) end
    end

    if sub_folder_lvl and sub_folder_lvl > 1 then

        sortFolders(folders)
        for k, v in pairs(folders) do
            local res, err = pcall(load_folder, path .. "/" .. v, sub_folder_lvl - 1)
            if res == false then print("sourcedev include error:\n" .. err) end
        end
    end
end


function load_sourcedev()
    load_folder("sourcedev", 5)
end

concommand.Add("sourcedev_reload", function(ply)
    if SERVER and IsValid(ply) and not ply:IsSuperAdmin() then return end

    load_sourcedev()
end)

load_sourcedev()